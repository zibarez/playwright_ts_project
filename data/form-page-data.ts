import { ru, Faker } from '@faker-js/faker';
import { randint } from '../config/helper';
import { format } from "date-fns";


export class FormPageLocators {
    FIRST_NAME_INPUT = '#firstname'
    LAST_NAME_INPUT = '#lasttname'
    EMAIL_INPUT = '#email'
    COUNTRY_CODE_SELECT = 'div[class="columns container"]:nth-child(2) select'
    COUNTRY_CODE_SELECT_OPTIONS = `${this.COUNTRY_CODE_SELECT} option`
    PHONE_NUMBER_INPUT = '#Phno'
    ADDRESS_INPUT_1 = '#Addl1'
    ADDRESS_INPUT_2 = '#Addl2'
    STATE_INPUT = '#state'
    POSTAL_CODE_INPUT = '#postalcode'
    COUNTRY_SELECT = 'div[class="columns container"]:nth-child(5) select'
    COUNTRY_SELECT_OPTIONS = `${this.COUNTRY_SELECT} option`
    DATE_OF_BIRTH = '#Date'
    MALE_RB = '#male'
    FEMALE_RB = '#female'
    TRANS_RB = '#trans'
    I_AGREE_CHECKBOX = '.checkbox input[type="checkbox"]'
    SUBMIT_BTN = 'input[type="submit"]'
}


export const faker: Faker = new Faker({
    locale: [ru],
})
    

export class FormPageData {
    locators: FormPageLocators = new FormPageLocators();
    async getRandomSex() {
        const randomSex = [this.locators.MALE_RB, this.locators.FEMALE_RB, this.locators.TRANS_RB];
        const randomIndex = randint(0, randomSex.length - 1);
        return randomSex[randomIndex];
    }

    SEX = this.getRandomSex();
    FIRST_NAME = faker.person.firstName();
    LAST_NAME = faker.person.lastName();
    EMAIL = faker.internet.email();
    PHONE = faker.phone.number().replace(/\D/g, '');
    ADDRESS_1 = faker.location.streetAddress();
    ADDRESS_2 = faker.location.secondaryAddress();
    POSTAL_CODE = faker.location.zipCode();
    STATE = faker.location.state();
    BIRTH_DAY = format(faker.date.birthdate(), 'ddMMyyyy');
    REQUIRED_ATTRIBUTE = 'required';
    TYPE_EMAIL = 'email';
    TYPE_TEXT = 'text';
    TYPE_PHONE = 'tel';
    TYPE_DATE = 'date';
    EMAIL_PATTERN = '[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$';
    PHONE_PATTERN = '[123456789][0-9]{9}';
}
