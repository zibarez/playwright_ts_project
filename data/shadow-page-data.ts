import { ru, Faker } from '@faker-js/faker';


export const ShadowPageLocators = {
    FIRST_NAME: '#fname',
    LAST_NAME: 'my-web-component',
    EMAIL: 'div[class="field"]:nth-child(3)',
}
    

export const faker: Faker = new Faker({
    locale: [ru],
})


export const ShadowPageData = {
    FIRST_NAME: faker.person.firstName(),
    LAST_NAME: faker.person.lastName(),
    EMAIL: faker.internet.email(),
}