import { Links } from '../config/links';


export const WindowsPageLocators = {
    BTN_OPEN_HOME_PAGE: '#home',
    BTN_MULTIPLE_WINDOWS: '#multi',
}


const links: Links = new Links()


export const WindowsPageData = {
    LIST_URLS: [
        links.WINDOWS,
        links.ALERT,
        links.SELECT,
    ]
}