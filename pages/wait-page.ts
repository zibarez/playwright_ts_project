import { allure } from "allure-playwright";
import { WaitPageLocators as locators } from '../data/wait-page-data';
import { BasePage } from './base-page';
import { Links } from '../config/links';
import { type Dialog } from "@playwright/test";


export class WaitPage extends BasePage {
    readonly links: Links = new Links();
    PAGE_URL: string = this.links.WAITS;


    async waitAndAcceptAlert() {
        await allure.step('Ожидание и принятие Alert', async() => {
            this.page.once('dialog', async (dialog: Dialog) => {
                await dialog.accept();
            });
            await this.page.locator(locators.ACCEPT_BTN).click();
            await this.page.waitForEvent('dialog', { timeout: 10000 });
        });
    }
}