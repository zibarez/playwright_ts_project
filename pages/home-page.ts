import { allure } from "allure-playwright";
import { BasePage } from './base-page';
import { ContentType } from "allure-js-commons";


export class HomePage extends BasePage {
    async printTitlePage() {
        await allure.step('Напечатать заголовок страницы', async() => {
            await this.pause(1000);
            let title: string = await this.page.title();
            console.log(title);
            await allure.attachment('Заголовок страницы', title, ContentType.TEXT);
        });
    }
}