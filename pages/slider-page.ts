import { allure } from "allure-playwright";
import { BasePage } from "./base-page";
import { SliderPageLocators as locators } from "../data/slider-page-data";
import { Links } from '../config/links';
import { randint } from '../config/helper'
import {expect, type Locator} from '@playwright/test'
type BoundingBox = { x: number; y: number; width: number; height: number } | null


export class SliderPage extends BasePage {
    readonly links: Links = new Links();
    randomSteps: number = randint(1, 50);
    PAGE_URL: string = this.links.SLIDER;

    async setSliderValue() {
        await allure.step(`Установить значение ${this.randomSteps} в слайдер`, async() => {
            const slider: Locator = this.page.locator(locators.SLIDER);
            const box: BoundingBox = await slider.boundingBox();
            const step: number = (box!.width * 0.98) / 50;
            await this.page.mouse.click(box!.x + this.randomSteps * step, box!.y + box!.height / 2);
        });
    }

    async clickBtnGetCountries() {
        await allure.step('Клик на кнопку "Get Countries"', async() => {
            await this.page.locator(locators.GET_COUNTRIES_BTN).click(); 
        });
    }

    async expectCountDisplayedCountries() {
        await allure.step('Проверка количества отображенных стран', async() => {
            await this.page.waitForSelector(locators.COUNTRIES_LIST);
            const countries: string | null = await this.page.locator(locators.COUNTRIES_LIST).textContent();
            expect(countries!.split(' - ').length).toBe(this.randomSteps);
        });
    }
}