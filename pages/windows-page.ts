import { allure } from "allure-playwright";
import { WindowsPageLocators as locators, WindowsPageData as data} from '../data/windows-page-data';
import { BasePage } from './base-page';
import {expect, BrowserContext, type Page } from '@playwright/test';
import { Links } from '../config/links';


export class WindowsPage extends BasePage {
    readonly links: Links = new Links();
    PAGE_URL: string = this.links.WINDOWS;

    async clickBtnOpenHomePage() {
        await allure.step('Клик на кнопку "Open Home Page"', async() => {
            await this.page.locator(locators.BTN_OPEN_HOME_PAGE).click();
        });
    }

    async waitNewTab(context: BrowserContext) {
        return allure.step('Ожидание открытия новой вкладки при клике на кнопку', async() => {
            const pagePromise: Promise<Page> = context.waitForEvent('page');
            await this.clickBtnOpenHomePage();
            const newPage: Page = await pagePromise;
            return newPage;
        });
    }

    async clickBtnMultipleWindows() {
        await allure.step('Клик на кнопку "Multiple Windows"', async() => {
            await this.page.locator(locators.BTN_MULTIPLE_WINDOWS).click();
            await this.page.waitForTimeout(500);
        });
    }

    async expectUrlOpenTabs(context: BrowserContext) {
        await allure.step('Проверка адресов открытых вкладок', async() => {
            const listTabs: Page[] = context.pages();
            for(let i: number = 0; i < listTabs.length; i++) {
                await expect(listTabs[i]).toHaveURL(data.LIST_URLS[i]);
            }
        });
    }

    async closeAllTabs(context: BrowserContext) {
        await allure.step('Закрытие всех вкладок', async() => {
            const listTabs: Page[] = context.pages();
            for(let i of listTabs) {
                await i.close();
            }
        });
    }
}