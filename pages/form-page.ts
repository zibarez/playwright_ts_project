import { allure } from "allure-playwright";
import { FormPageLocators, FormPageData } from '../data/form-page-data';
import { BasePage } from './base-page';
import { expect, type Locator } from '@playwright/test';
import { Links } from '../config/links';
import { randint } from "../config/helper";


export class FormPage extends BasePage {
    readonly links: Links = new Links();
    data: FormPageData = new FormPageData();
    locators: FormPageLocators = new FormPageLocators();
    PAGE_URL: string = this.links.FORMS;
    countryCodeValue: any;
    countryValue: any;

    async expectFieldIsRequired(selector: string) {
        await allure.step(`Проверка поля ${selector} на обязтельность`, async() => {
            await expect.soft(this.page.locator(selector)).toHaveAttribute(this.data.REQUIRED_ATTRIBUTE, '');
        });
    }

    async expectInputType(selector: string, inputType: string) {
        await allure.step(`Проверка типа поля: ${selector}`, async() => {
            const attr: string | null = await this.page.locator(selector).getAttribute('type');
            expect.soft(attr).toEqual(inputType);
        });
    }

    async expectInputPattern(selector: string, pattern: string) {
        await allure.step(`Проверка наличия паттерна ${pattern} у поля ${selector}`, async() => {
            await expect.soft(this.page.locator(selector)).toHaveAttribute('pattern', pattern);
        });
    }

    async fillFirstName() {
        await allure.step('Ввод имени', async() => {
            await this.page.locator(this.locators.FIRST_NAME_INPUT).fill(this.data.FIRST_NAME);
        });
    }

    async fillLastName() {
        await allure.step('Ввод фамилии', async() => {
            await this.page.locator(this.locators.LAST_NAME_INPUT).fill(this.data.LAST_NAME);
        });
    }

    async fillEmail() {
        await allure.step('Ввод электронной почты', async() => {
            await this.page.locator(this.locators.EMAIL_INPUT).fill(this.data.EMAIL);
        });
    }

    async fillPhoneNumber() {
        await allure.step('Ввод телефонного номера', async() => {
            await this.page.locator(this.locators.PHONE_NUMBER_INPUT).fill(this.data.PHONE);
        });
    }

    async fillAddressFirst() {
        await allure.step('Ввод адреса №1', async() => {
            await this.page.locator(this.locators.ADDRESS_INPUT_1).fill(this.data.ADDRESS_1);
        });
    }

    async fillAddressSecond() {
        await allure.step('Ввод адреса №2', async() => {
            await this.page.locator(this.locators.ADDRESS_INPUT_2).fill(this.data.ADDRESS_2);
        });
    }

    async fillState() {
        await allure.step('Ввод названия адмнистративной единицы (область)', async() => {
            await this.page.locator(this.locators.STATE_INPUT).fill(this.data.STATE);
        });
    }

    async fillPostalCode() {
        await allure.step('Ввод почтового индекса', async() => {
            await this.page.locator(this.locators.POSTAL_CODE_INPUT).fill(this.data.POSTAL_CODE);
        });
    }

    async fillBirthday() {
        await allure.step('Ввод даты рождения', async() => {
            await this.page.locator(this.locators.DATE_OF_BIRTH).pressSequentially(this.data.BIRTH_DAY)
        });
    }

    async setSex() {
        await allure.step('Установить пол', async() => {
            const sex: string = await this.data.SEX;
            await this.page.locator(sex).click();
        });
    }

    async setConutryCode() {
        await allure.step('Установить код страны', async() => {
            const options: Locator[] = await this.page.locator(this.locators.COUNTRY_CODE_SELECT_OPTIONS).all();
            const option: number = randint(0, options.length - 1);
            this.countryCodeValue ??= await options[option].getAttribute('value');
            await this.page.locator(this.locators.COUNTRY_CODE_SELECT).selectOption( { value: this.countryCodeValue } );
        });
    }

    async expectCountryCode() {
        await allure.step('Проверка выбранного кода страны', async() => {
            await expect(this.page.locator(this.locators.COUNTRY_CODE_SELECT)).toHaveValue(this.countryCodeValue);
        });
    }

    async setCountry() {
        await allure.step('Установить страну', async() => {
            const options: Locator[] = await this.page.locator(this.locators.COUNTRY_SELECT_OPTIONS).all();
            const option: number = randint(0, options.length - 1);
            this.countryValue ??= await options[option].getAttribute('value');
            await this.page.locator(this.locators.COUNTRY_SELECT).selectOption( { value: this.countryValue } );
        });
    }

    async expectCountry() {
        await allure.step('Проверка выбранной страны', async() => {
            await expect(this.page.locator(this.locators.COUNTRY_SELECT)).toHaveValue(this.countryValue);
        });
    }

    async checkCheckboxIAgree() {
        await allure.step('Установить чек-бокс "I agree..."', async() => {
            await this.page.locator(this.locators.I_AGREE_CHECKBOX).check();
        });
    }

    async clickBtnSubmit() {
        await allure.step('Клик на кнопку "Отправить"', async() => {
            await this.page.locator(this.locators.SUBMIT_BTN).click();
        });
    }
}