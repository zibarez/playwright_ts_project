import { allure } from "allure-playwright";
import { BasePage } from "./base-page";
import { TablePageLocators as locators, TablePageData as data } from "../data/table-page-data";
import { Links } from '../config/links';
import {expect, type Locator} from '@playwright/test'


export class TablePage extends BasePage {
    readonly links: Links = new Links();
    summAllPrice: number = 0
    PAGE_URL: string = this.links.TABLE;

    async getSummAllPrice() {
        await allure.step('Получение суммы всех товаров из таблицы "Shopping List"', async() => {
            const cells_table: Locator[] = await this.page.locator(locators.CELLS_SHOPPING_LIST_TABLE).all();
            for (let i: number = 0; i < cells_table.length; i++) {
                if (i % 2 == 0) { }
                else {
                    this.summAllPrice += Number(await cells_table[i].textContent());
                }
            }
        });
            return this.summAllPrice;
    }

    async expectTotalPrice() {
        await allure.step('Проверка значения суммы в таблице', async() => {
            const total: Locator = this.page.locator(locators.FOOTER_SHOPPING_LIST_TABLE).last()
            expect(Number(await total.textContent())).toBe(await this.getSummAllPrice());
        });
    }

    async checkRowWithRajName() {
        await allure.step('Отметить чек-бокс в строке с именем Raj', async() => {
            const rows: Locator[] = await this.page.locator(locators.ROWS_TABLE_LETS_HANDLE_IT).all();
            for (let row of rows) {
                let rowText: String[] = await row.allInnerTexts()
                let splitRowText: String[] = rowText[0].split('\t')
                if (splitRowText.includes(data.RAJ_NAME)){
                    await row.getByRole('checkbox').check();
                }
            }
        });
    }

    async sortColumn(column: string, direction: string) {
        await allure.step(`Отсортировать столбец ${column} по направлению ${direction}`, async() => {
            const header: Locator = this.page.locator(locators.HEADERS_COLUMNS).filter( {hasText: column} );
            if (direction == data.ASCENDING) {
                while (await header.getAttribute(data.SORT_ATTRIBUTE) != data.ASCENDING) {
                    await header.click();
                }
            }
            else if (direction == data.DESCENDING) {
                while (await header.getAttribute(data.SORT_ATTRIBUTE) != data.DESCENDING) {
                    await header.click();
                }
            }
        });
    }

    async expectSortColumn(column: string, direction: string) {
        await allure.step(`Проверка сортировки столбца ${column} в направлении ${direction}`, async() => {
            let index: number = 0;
            switch (column) {
                case data.DESSERT:
                    index = 0;
                    break;
                case data.CALORIES:
                    index = 1;
                    break;
                case data.FAT:
                    index = 2;
                    break;
                case data.CARBS:
                    index = 3;
                    break;
                case data.PROTEIN:
                    index = 4;
                    break;
                case data.CHOLESTEROL:
                    index = 5;
                    break;
            }
            let col: string[] = [];
            const rows: Locator[] = await this.page.locator(locators.ROWS_SORT_TABLE).all();
            for (let row of rows) {
                let innerText: string[] = await row.allInnerTexts();
                let splitInnerText: string[] = innerText[0].split('\t\n')
                col!.push(splitInnerText[index]);
            }
            if (direction == data.ASCENDING) {
                const sortCol: string[] = [...col].sort((a: string, b: string) => (a < b ? -1 : 1));
                expect(col).toEqual(sortCol);
            }
            else if (direction == data.DESCENDING) {
                const reverseSortCol: string[] = [...col].sort((a: string, b: string) => (a > b ? -1 : 1));
                expect(col).toEqual(reverseSortCol);
            }
        });
    }
} 