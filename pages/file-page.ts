import { allure } from "allure-playwright";
import { FilePageLocators as locators, FilePageData as data } from '../data/file-page-data';
import { BasePage } from './base-page';
import { Links } from '../config/links';
import { join } from 'path'
import { fileExists, clearDownloadDir } from '../config/helper'
import { expect } from '@playwright/test'


export class FilePage extends BasePage {
    readonly links: Links = new Links();
    PAGE_URL: string = this.links.FILE;
    readonly ROOT_DIR: string = process.cwd();

    async downloadExcelFile() {
        await allure.step('Загрузить Excel файл', async() => {
            const downloadPromise = this.page.waitForEvent('download');
            await this.page.getByRole("link", { name: data.DOWNLOAD_EXCEL_NAME }).click();
            const download = await downloadPromise;
            await download.saveAs(join(this.ROOT_DIR, data.DOWNLOAD_DIR, download.suggestedFilename()));
        });
    }

    async expectDownloadingExcelFile() {
        await allure.step('Проверка загрузки Excel файла', async() => {
            const exists: boolean = await fileExists(join(this.ROOT_DIR, data.DOWNLOAD_DIR, data.EXCEL_NAME))
            expect(exists).toEqual(true);
        });
    }

    async downloadPdfFile() {
        await allure.step('Загрузить PDF файл', async() => {
            const downloadPromise = this.page.waitForEvent('download');
            await this.page.getByRole("link", { name: data.DOWNLOAD_PDF_NAME }).click();
            const download = await downloadPromise;
            await download.saveAs(join(this.ROOT_DIR, data.DOWNLOAD_DIR, download.suggestedFilename()));
        });
    }

    async expectDownloadingPdfile() {
        await allure.step('Проверка загрузки PDF файла', async() => {
            const exists: boolean = await fileExists(join(this.ROOT_DIR, data.DOWNLOAD_DIR, data.PDF_NAME))
            expect(exists).toEqual(true);
        });
    }

    async downloadTxtFile() {
        await allure.step('Загрузить txt файл', async() => {
            const downloadPromise = this.page.waitForEvent('download');
            await this.page.getByRole("link", { name: data.DOWNLOAD_TEXT_NAME }).click();
            const download = await downloadPromise;
            await download.saveAs(join(this.ROOT_DIR, data.DOWNLOAD_DIR, download.suggestedFilename()));
        });
    }

    async expectDownloadingTxtile() {
        await allure.step('Проверка загрузки PDF файла', async() => {
            const exists: boolean = await fileExists(join(this.ROOT_DIR, data.DOWNLOAD_DIR, data.TEXT_NAME))
            expect(exists).toEqual(true);
        });
    }

    async removeFilesFromDownloadsDir() {
        await allure.step('Удаление скачанных файлов из папки "downloads"', async() => {
            await clearDownloadDir();
        });
    }

    async uploadFile() {
        await allure.step('Загрузить файл', async() => {
            await this.page.locator(locators.UPLOAD)
            .setInputFiles(join(this.ROOT_DIR, data.UPLOAD_DIR, data.TEST_FILE_NAME));
        });
    }
}