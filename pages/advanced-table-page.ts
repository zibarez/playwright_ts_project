import { allure } from "allure-playwright";
import { BasePage } from "./base-page";
import {expect, type Locator} from '@playwright/test';
import { AdvancedTablePageLocators as locators, AdvancedTablePageData as data } from "../data/advanced-table-page-data";
import { Links } from '../config/links';
import { randint } from '../config/helper'


export class AdvancedTablePage extends BasePage {
    readonly links: Links = new Links();
    PAGE_URL: string = this.links.ADVANCED_TABLE;
    searchRequest: string = data.SEARCH_PATTERN[randint(0, data.SEARCH_PATTERN.length - 1)]

    async fillSearchRequest() {
        await allure.step(`Ввод поискового запроса: ${this.searchRequest}`, async() => {
            await this.page.locator(locators.INPUT_SEARCH).fill(this.searchRequest);
            await this.pause(500);
        });
    }

    async expectSearchRowNumbers(arr: number[]) {
        await allure.step('Проверка номеров найденных строк', async() => {
            const rows: Locator[] = await this.page.locator(locators.ROWS_TABLE).all();
            for (let i: number = 0; i < arr.length; i++) {
                let innerText: string[] = await rows[i].allInnerTexts();
                let splitInnerText: string = innerText[0].split('\t')[0];
                expect(splitInnerText).toEqual(String(arr[i]));
            }
        });
    }

    async expectSearchResults() {
        if (this.searchRequest == data.SEARCH_PATTERN[0]) {
            await this.expectSearchRowNumbers(data.AMERICAN_NO);
        }
        else if (this.searchRequest == data.SEARCH_PATTERN[1]) {
            await this.expectSearchRowNumbers(data.EDU_NO);
        }
        else if (this.searchRequest == data.SEARCH_PATTERN[2]) {
            await this.chooseCountRowsPerPage(data.COUNT_25);
            await this.expectSearchRowNumbers(data.LONDON_NO);
        }
        else if (this.searchRequest == data.SEARCH_PATTERN[3]) {
            await this.expectSearchRowNumbers(data.NO_35);
        }
    }

    async chooseCountRowsPerPage(count: string) {
        await allure.step('Выбрать количество отображаемых строк таблицы на странице', async() => {
            await this.page.locator(locators.SELECT_COUNT_ROWS).selectOption(count);
        });
    }

    async expectCountRows(count: string) {
        await allure.step(`Проверка количества отображаемых в таблице строк (${count})`, async() => {
            await this.chooseCountRowsPerPage(count);
            const rows: Locator[] = await this.page.locator(locators.ROWS_TABLE).all();
            expect(String(rows.length)).toEqual(count);
        });
    }

    async getLocatorsPageButtons() {
        return allure.step('Получить список локаторов кнопок страниц', async() => {
            let btns: Locator[] = []
            const buttons: Locator[] = await this.page.locator(locators.PAGE_BUTTONS).all();
            for (let btn of buttons) {
                let classAttribute: string | null = await btn.getAttribute(data.CLASS);
                if (data.CLASS_CURRENT_PAGE != classAttribute) {
                    btns.push(btn);
                }
            }
            return btns;
        });
    }

    async goToRandomPage() {
        await allure.step('Перейти на случайную страницу из доступных и проверить корректность перехода', async() => {
            const buttons: Locator[] = await this.getLocatorsPageButtons();
            if (buttons.length != 0) {
                const btnNumber: number = randint(0, buttons.length - 1);
                await buttons[btnNumber].click();
                await this.pause(100);
                await expect(buttons[btnNumber]).toHaveClass(data.CLASS_CURRENT_PAGE);
            }
        });
    }

    async getNumberCurentPage() {
        return allure.step('Получить номер текущей страницы', async() => {
            const buttons: Locator[] = await this.page.locator(locators.PAGE_BUTTONS).all();
            let count: number = 0;
            for (let btn of buttons) {
                let classAttribute: string | null = await btn.getAttribute(data.CLASS);
                if (data.CLASS_CURRENT_PAGE == classAttribute) {
                    count = Number(await btn.textContent());
                    break;
                }
            }
            return count;
        });
    }

    async clickBtnNext() {
        await allure.step('Нажать на кнопку "Next"', async() => {
            await this.page.locator(locators.NEXT_BTN).click();
        });
    }

    async expectNextButton() {
        await allure.step('Проверка работы кнопки "Next"', async() => {
            let before: number = await this.getNumberCurentPage();
            await this.clickBtnNext();
            let after: number = await this.getNumberCurentPage();
            expect(before + 1).toEqual(after)
        });
    }

    async clickBtnLast() {
        await allure.step('Нажать на кнопку "Last"', async() => {
            await this.page.locator(locators.LAST_BTN).click();
        });
    }

    async clickBtnFirst() {
        await allure.step('Нажать на кнопку "First"', async() => {
            await this.page.locator(locators.FIRST_BTN).click();
        });
    }

    async clickBtnPrevious() {
        await allure.step('Нажать на кнопку "Previous"', async() => {
            await this.page.locator(locators.PREVIOUS_BTN).click();
        });
    }

    async expectLastButton() {
        await allure.step('Проверка работы кнопки "Last"', async() => {
            const buttons: Locator[] = await this.getLocatorsPageButtons();
            let numberLastPage: string | null | undefined = '';
            if (buttons.length != 0) {
                numberLastPage = await buttons.at(-1)?.textContent();
            }
            await this.clickBtnLast();
            const numberCurrentPage: number = await this.getNumberCurentPage();
            expect(Number(numberLastPage)).toEqual(numberCurrentPage)
        });
    }

    async expectPreviousButton() {
        await allure.step('Проверка работы кнопки "Previous"', async() => {
            await this.clickBtnLast();
            const before: number = await this.getNumberCurentPage();
            await this.clickBtnPrevious();
            const after: number = await this.getNumberCurentPage();
            expect(before - 1).toEqual(after)
        });
    }

    async expectFirstButton() {
        await allure.step('Проверка работы кнопки "First"', async() => {
            await this.clickBtnLast();
            await this.clickBtnFirst();
            const currentPageNumber = await this.getNumberCurentPage();
            expect(currentPageNumber).toEqual(1)
        });
    }

    async sortColumn(column: string, direction: string) {
        await allure.step(`Отсортировать столбец ${column} по направлению ${direction}`, async() => {
            const header: Locator = this.page.getByLabel(column);
            if (direction == data.ASCENDING) {
                while (await header.getAttribute(data.SORT_ATTRIBUTE) != data.ASCENDING) {
                    await header.click();
                }
            }
            else if (direction == data.DESCENDING) {
                while (await header.getAttribute(data.SORT_ATTRIBUTE) != data.DESCENDING) {
                    await header.click();
                }
            }
        });
    }

    async expectSortColumn(column: string, direction: string) {
        await allure.step(`Проверка сортировки стобца ${column} в направлении ${direction}`, async() => {
            let index: number = 0;
            switch (column) {
                case data.S_NO:
                    index = 0;
                    break;
                case data.UNIVERSITY_NAME:
                    index = 1;
                    break;
                case data.COUNTRY:
                    index = 2;
                    break;
                case data.WEBSITE:
                    index = 3;
                    break;
            }
            let col: string[] = [];
            const rows: Locator[] = await this.page.locator(locators.ROWS_TABLE).all();
            for (let row of rows) {
                let innerText: string[] = await row.allInnerTexts();
                let splitInnerText: string[] = innerText[0].split('\t')
                col!.push(splitInnerText[index]);
            }
            if (direction == data.ASCENDING) {
                const sortCol: string[] = [...col].sort((a: string, b: string) => (a < b ? -1 : 1));
                expect(col).toEqual(sortCol);
            }
            else if (direction == data.DESCENDING) {
                const reverseSortCol: string[] = [...col].sort((a: string, b: string) => (a > b ? -1 : 1));
                expect(col).toEqual(reverseSortCol);
            }
        });
    }
}