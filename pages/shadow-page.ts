import { allure } from "allure-playwright";
import { ShadowPageLocators as locators, ShadowPageData as data } from '../data/shadow-page-data';
import { BasePage } from './base-page';
import { Links } from '../config/links';


export class ShadowPage extends BasePage {
    readonly links: Links = new Links();
    PAGE_URL: string = this.links.SHADOW;

    async inputFirstName() {
        await allure.step('Ввод имени', async() => {
            await this.page.locator(locators.FIRST_NAME).fill(data.FIRST_NAME);
        });
    }

    async inputLastName() {
        await allure.step('Ввод фамилии', async() => {
            await this.page.locator(locators.FIRST_NAME).focus();
            await this.page.keyboard.press('Tab');
            await this.page.locator(locators.LAST_NAME).pressSequentially(data.LAST_NAME);
        });
    }

    async inputEmail() {
        await allure.step('Ввод электронной почты', async() => {
            await this.page.locator(locators.FIRST_NAME).focus();
            await this.page.keyboard.press('Tab');
            await this.page.keyboard.press('Tab');
            await this.page.locator(locators.EMAIL).pressSequentially(data.EMAIL);
        });
    }
}