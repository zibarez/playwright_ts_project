import { allure } from "allure-playwright";
import { DragPageLocators as locators, DragPageData as data} from '../data/drag-page-data';
import { BasePage } from './base-page';
import {expect, type Locator} from '@playwright/test';
import { Links } from '../config/links';
type BoundingBox = { x: number; y: number; width: number; height: number } | null


export class DragPage extends BasePage {
    readonly links: Links = new Links();
    PAGE_URL: string = this.links.DRAG;

    async dragAndDropBox() {
        await allure.step('Drag and Drop box', async() => {
            const box: BoundingBox = await this.page.locator(locators.DRAG_BOX).boundingBox();
            const dragBox: Locator = this.page.locator(locators.DRAG_BOX);
            await dragBox.hover();
            await this.page.mouse.down();
            await this.page.mouse.move(box!.x + 300, box!.y + 300);
            await this.page.mouse.move(box!.x + 300, box!.y + 300);
            await this.page.mouse.up();
        });
    }

    async expectDraggedBox() {
        await allure.step('Проверка перемещения квадрата', async() => {
            const dragBox: Locator = this.page.locator(locators.DRAG_BOX);
            await expect(dragBox).toHaveAttribute(data.ATTRIBUTE, data.DRAG_BOX_STYLE_AFTER_DRAG);
        });
    }
}