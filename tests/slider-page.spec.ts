import { test } from '@playwright/test';
import { SliderPage } from '../pages/slider-page';


test('Тест Select Page', async({page}) => {
    const sliderPage: SliderPage = new SliderPage(page);
    await sliderPage.navigate(sliderPage.PAGE_URL);
    await sliderPage.setSliderValue();
    await sliderPage.clickBtnGetCountries();
    await sliderPage.expectCountDisplayedCountries();
})