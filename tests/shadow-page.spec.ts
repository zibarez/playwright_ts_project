import { test } from '@playwright/test';
import { ShadowPage } from '../pages/shadow-page';

test('Тест Shadow Page', async( {page} ) => {
    const shadowPage: ShadowPage = new ShadowPage(page);
    await shadowPage.navigate(shadowPage.PAGE_URL);
    await shadowPage.inputFirstName();
    await shadowPage.inputLastName();
    await shadowPage.inputEmail();
})