import { test } from '@playwright/test';
import { CalendarPage } from '../pages/calendar-page';

test('Тест Calendar Page', async( {page} ) => {
    const calendarPage: CalendarPage = new CalendarPage(page);
    await calendarPage.navigate(calendarPage.PAGE_URL);
    await calendarPage.chooseStartDateAsToday();
    await calendarPage.chooseDateAfterThreeDays();
    await calendarPage.expectChosenDates();
    await calendarPage.chooseTodayDateTime();
    await calendarPage.increaseTimeTwoHours();
    await calendarPage.expectDateTime();
})