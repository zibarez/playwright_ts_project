import { test } from '@playwright/test';
import { DragPage } from '../pages/drag-page';

test('Тест Drag Page', async({ page }) => {
    const dragPage: DragPage = new DragPage(page);
    await dragPage.navigate(dragPage.PAGE_URL);
    await dragPage.dragAndDropBox();
    await dragPage.expectDraggedBox();
})