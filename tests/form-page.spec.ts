import { test } from '@playwright/test';
import { FormPage } from '../pages/form-page';
import { FormPageLocators, FormPageData } from '../data/form-page-data';


test('Тест Elements Page Validate', async({ page }) => {
    const data: FormPageData = new FormPageData();
    const locators: FormPageLocators = new FormPageLocators();
    const formPage: FormPage = new FormPage(page);
    await formPage.navigate(formPage.PAGE_URL);
    await formPage.expectFieldIsRequired(locators.FIRST_NAME_INPUT);
    await formPage.expectFieldIsRequired(locators.LAST_NAME_INPUT);
    await formPage.expectFieldIsRequired(locators.EMAIL_INPUT);
    await formPage.expectFieldIsRequired(locators.PHONE_NUMBER_INPUT);
    await formPage.expectFieldIsRequired(locators.ADDRESS_INPUT_1);
    await formPage.expectFieldIsRequired(locators.ADDRESS_INPUT_2);
    await formPage.expectFieldIsRequired(locators.STATE_INPUT);
    await formPage.expectFieldIsRequired(locators.POSTAL_CODE_INPUT);
    await formPage.expectFieldIsRequired(locators.DATE_OF_BIRTH);
    await formPage.expectFieldIsRequired(locators.I_AGREE_CHECKBOX);
    await formPage.expectInputType(locators.FIRST_NAME_INPUT, data.TYPE_TEXT);
    await formPage.expectInputType(locators.LAST_NAME_INPUT, data.TYPE_TEXT);
    await formPage.expectInputType(locators.EMAIL_INPUT, data.TYPE_EMAIL);
    await formPage.expectInputType(locators.PHONE_NUMBER_INPUT, data.TYPE_PHONE);
    await formPage.expectInputType(locators.ADDRESS_INPUT_2, data.TYPE_TEXT);
    await formPage.expectInputType(locators.ADDRESS_INPUT_1, data.TYPE_TEXT);
    await formPage.expectInputType(locators.STATE_INPUT, data.TYPE_TEXT);
    await formPage.expectInputType(locators.POSTAL_CODE_INPUT, data.TYPE_TEXT);
    await formPage.expectInputType(locators.FIRST_NAME_INPUT, data.TYPE_TEXT);
    await formPage.expectInputType(locators.DATE_OF_BIRTH, data.TYPE_DATE);
    await formPage.expectInputPattern(locators.EMAIL_INPUT, data.EMAIL_PATTERN);
    await formPage.expectInputPattern(locators.PHONE_NUMBER_INPUT, data.PHONE_PATTERN);
})

test('Тест Elements Page Fill Fields', async({ page }) => {
    const formPage: FormPage = new FormPage(page);
    await formPage.navigate(formPage.PAGE_URL);
    await formPage.fillFirstName();
    await formPage.fillLastName();
    await formPage.fillEmail();
    await formPage.fillPhoneNumber();
    await formPage.fillAddressFirst();
    await formPage.fillAddressSecond();
    await formPage.fillState();
    await formPage.fillPostalCode();
    await formPage.fillBirthday();
    await formPage.setSex();
    await formPage.setConutryCode();
    await formPage.expectCountryCode();
    await formPage.setCountry();
    await formPage.expectCountry();
    await formPage.checkCheckboxIAgree();
    await formPage.clickBtnSubmit();
})