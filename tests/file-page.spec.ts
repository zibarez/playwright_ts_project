import { test } from '@playwright/test';
import { FilePage } from '../pages/file-page';

test('Тест File Page Downloads', async({ page }) => {
    const filePage: FilePage = new FilePage(page);
    await filePage.navigate(filePage.PAGE_URL);
    await filePage.downloadExcelFile();
    await filePage.downloadPdfFile();
    await filePage.downloadTxtFile();
    await filePage.expectDownloadingExcelFile();
    await filePage.expectDownloadingPdfile();
    await filePage.expectDownloadingTxtile();
    await filePage.removeFilesFromDownloadsDir();
})

test('Тест File Page Upload', async({ page }) => {
    const filePage: FilePage = new FilePage(page);
    await filePage.navigate(filePage.PAGE_URL);
    await filePage.uploadFile();
})
