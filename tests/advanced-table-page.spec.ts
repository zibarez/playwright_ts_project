import { test } from '@playwright/test';
import { AdvancedTablePage } from '../pages/advanced-table-page';
import { AdvancedTablePageData as data } from "../data/advanced-table-page-data";


test('Тест Advanced Table Page Search', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.fillSearchRequest();
    await advancedTablePage.expectSearchResults();
})

test('Тест Advanced Table Page Count Rows', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.expectCountRows(data.COUNT_10);
    await advancedTablePage.expectCountRows(data.COUNT_5);
    await advancedTablePage.expectCountRows(data.COUNT_25);
})

test('Тест Advanced Table Page Switch Page', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.goToRandomPage();
})

test('Тест Advanced Table Page Btn Next', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.expectNextButton();
    await advancedTablePage.expectNextButton();
})

test('Тест Advanced Table Page Btn Last', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.expectLastButton();
})

test('Тест Advanced Table Page Btn Previous', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.expectPreviousButton();
})

test('Тест Advanced Table Page Btn First', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.expectFirstButton();
})

test('Тест Advanced Table Page Sort S_NO', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.sortColumn(data.S_NO, data.DESCENDING);
    await advancedTablePage.expectSortColumn(data.S_NO, data.DESCENDING);
    await advancedTablePage.sortColumn(data.S_NO, data.ASCENDING);
    await advancedTablePage.expectSortColumn(data.S_NO, data.ASCENDING);
})

test('Тест Advanced Table Page Sort University', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.sortColumn(data.UNIVERSITY_NAME, data.DESCENDING);
    await advancedTablePage.expectSortColumn(data.UNIVERSITY_NAME, data.DESCENDING);
    await advancedTablePage.sortColumn(data.UNIVERSITY_NAME, data.ASCENDING);
    await advancedTablePage.expectSortColumn(data.UNIVERSITY_NAME, data.ASCENDING);
})

test('Тест Advanced Table Page Sort Country', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.sortColumn(data.COUNTRY, data.DESCENDING);
    await advancedTablePage.expectSortColumn(data.COUNTRY, data.DESCENDING);
    await advancedTablePage.sortColumn(data.COUNTRY, data.ASCENDING);
    await advancedTablePage.expectSortColumn(data.COUNTRY, data.ASCENDING);
})

test('Тест Advanced Table Page Sort Website', async( {page} ) => {
    const advancedTablePage: AdvancedTablePage = new AdvancedTablePage(page);
    await advancedTablePage.navigate(advancedTablePage.PAGE_URL);
    await advancedTablePage.sortColumn(data.WEBSITE, data.DESCENDING);
    await advancedTablePage.expectSortColumn(data.WEBSITE, data.DESCENDING);
    await advancedTablePage.sortColumn(data.WEBSITE, data.ASCENDING);
    await advancedTablePage.expectSortColumn(data.WEBSITE, data.ASCENDING);
})
