import { test } from '@playwright/test';
import { DropPage } from '../pages/drop-page';

test('Тест Drop Page', async({ page }) => {
    const dropPage: DropPage = new DropPage(page);
    await dropPage.navigate(dropPage.PAGE_URL);
    await dropPage.expectTextBeforeDragAndDrop();
    await dropPage.dragAndDropFirstBoxToSecondBox();
    await dropPage.expectTextAfterDragAndDrop();
})