import {type Page, test} from '@playwright/test';
import { WindowsPage } from '../pages/windows-page';
import { HomePage } from '../pages/home-page';


test('Тест Windows Page > Home Page', async({context}) => {
    const page: Page = await context.newPage()
    const windowsPage: WindowsPage = new WindowsPage(page);
    await windowsPage.navigate(windowsPage.PAGE_URL);
    const newPage: Page = await windowsPage.waitNewTab(context);
    const homePage: HomePage = new HomePage(newPage);
    await homePage.printTitlePage();
    await windowsPage.close();
    await homePage.close();
})

test('Тест Windows Page > Multiple Windows', async({context}) => {
    const page: Page = await context.newPage()
    const windowsPage: WindowsPage = new WindowsPage(page);
    await windowsPage.navigate(windowsPage.PAGE_URL);
    await windowsPage.clickBtnMultipleWindows();
    await windowsPage.expectUrlOpenTabs(context);
    await windowsPage.closeAllTabs(context);
})