import { test } from '@playwright/test';
import { TablePage } from '../pages/table-page';
import { TablePageData as data } from '../data/table-page-data'


test('Тест Table Page таблица "Shopping List"', async({page}) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.expectTotalPrice();
})

test('Тест Table Page таблица "Lets handle it"', async({page}) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.checkRowWithRajName();
})

test(`Тест Table Page таблица "Sortable Tables", столбец ${data.DESSERT}`, async( {page} ) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.sortColumn(data.DESSERT, data.DESCENDING);
    await tablePage.expectSortColumn(data.DESSERT, data.DESCENDING);
    await tablePage.sortColumn(data.DESSERT, data.ASCENDING);
    await tablePage.expectSortColumn(data.DESSERT, data.ASCENDING);
})

test(`Тест Table Page таблица "Sortable Tables", столбец ${data.CALORIES}`, async( {page} ) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.sortColumn(data.CALORIES, data.DESCENDING);
    await tablePage.expectSortColumn(data.CALORIES, data.DESCENDING);
    await tablePage.sortColumn(data.CALORIES, data.ASCENDING);
    await tablePage.expectSortColumn(data.CALORIES, data.ASCENDING);
})

test(`Тест Table Page таблица "Sortable Tables", столбец ${data.FAT}`, async( {page} ) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.sortColumn(data.FAT, data.DESCENDING);
    await tablePage.expectSortColumn(data.FAT, data.DESCENDING);
    await tablePage.sortColumn(data.FAT, data.ASCENDING);
    await tablePage.expectSortColumn(data.FAT, data.ASCENDING);
})

test(`Тест Table Page таблица "Sortable Tables", столбец ${data.CARBS}`, async( {page} ) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.sortColumn(data.CARBS, data.DESCENDING);
    await tablePage.expectSortColumn(data.CARBS, data.DESCENDING);
    await tablePage.sortColumn(data.CARBS, data.ASCENDING);
    await tablePage.expectSortColumn(data.CARBS, data.ASCENDING);
})

test(`Тест Table Page таблица "Sortable Tables", столбец ${data.PROTEIN}`, async( {page} ) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.sortColumn(data.PROTEIN, data.DESCENDING);
    await tablePage.expectSortColumn(data.PROTEIN, data.DESCENDING);
    await tablePage.sortColumn(data.PROTEIN, data.ASCENDING);
    await tablePage.expectSortColumn(data.PROTEIN, data.ASCENDING);
})

test(`Тест Table Page таблица "Sortable Tables", столбец ${data.CHOLESTEROL}`, async( {page} ) => {
    const tablePage: TablePage = new TablePage(page);
    await tablePage.navigate(tablePage.PAGE_URL);
    await tablePage.sortColumn(data.CHOLESTEROL, data.DESCENDING);
    await tablePage.expectSortColumn(data.CHOLESTEROL, data.DESCENDING);
    await tablePage.sortColumn(data.CHOLESTEROL, data.ASCENDING);
    await tablePage.expectSortColumn(data.CHOLESTEROL, data.ASCENDING);
})
