import fs from 'fs/promises';
import { join } from 'path';


export function randint(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export async function fileExists(path: string): Promise<boolean> {
    try{
        await fs.access(path, fs.constants.F_OK);
        return true;
    } catch {
        return false;
    }
}

export async function clearDownloadDir() {
    const downloadPath: string = join(process.cwd(), 'downloads');
    await fs.rmdir(downloadPath, { recursive: true });
    await fs.mkdir(downloadPath);
}